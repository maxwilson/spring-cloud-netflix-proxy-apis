package br.com.emmanuelneri;

import com.google.common.collect.ImmutableMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/sallers")
public class SallerController {

    private static final Map<Long, String> SALLERS = ImmutableMap.of(
            1L, "Miguel ",
            2L, "Bruno",
            3L, "Brena",
            4L, "Carlos");

    @GetMapping
    public List<String> findAll() {
    	return new ArrayList<>(SALLERS.values());
    }

    @GetMapping(path = "/{id}")
    public String findById(@PathVariable("id") Long id) {
        return SALLERS.get(id);
    }

}
