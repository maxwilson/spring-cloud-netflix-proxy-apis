package br.com.emmanuelneri;

import com.google.common.collect.ImmutableMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/contacts")
public class ContactController {

    private static final Map<Long, String> Contacts = ImmutableMap.of(
            1L, "1 - (11)123456789",
            2L, "2 - (11)123456789",
            3L, "3 - (11)123456789",
            4L, "4 - (11)123456789");

    @GetMapping
    public List<String> findAll() {
        return new ArrayList<>(Contacts.values());
    }

    @GetMapping(path = "/{id}")
    public String findById(@PathVariable("id") Long id) {
    	return Contacts.get(id);
    }

}
