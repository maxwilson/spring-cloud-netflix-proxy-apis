package activemq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;


@Component
public class ConsomeMensagemListener {

    Logger log = LoggerFactory.getLogger(ConsomeMensagemListener.class);

    @JmsListener(destination = "br.com.fujideia.topico.usuario")
    public void processMessage(Usuario usuario) {

        log.info("Recebendo mensagem por Listener {} ", usuario.getNome());
    }

}
