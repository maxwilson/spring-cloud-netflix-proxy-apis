package activemq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
@EnableAsync
public class AgendaConsumo {

    @Autowired
    private JmsTemplate jmsTemplate;

    Logger log = LoggerFactory.getLogger(AgendaConsumo.class);

    @Scheduled(fixedRate = 10000)
    @Async
    public void consomeFila(){

        try{
            log.info("Schedule - Processamento de usuário Iniciado para  "+ Destinations.FILA_USUARIO);
            Usuario usuario = (Usuario) jmsTemplate.receiveAndConvert(Destinations.FILA_USUARIO);
            log.info("Usuario recebido pela Schedule : {}" , usuario.getNome());
        } catch(Exception e){
            log.error(" Usuario recebido pela Schedule: ", e);
        }
    }
}