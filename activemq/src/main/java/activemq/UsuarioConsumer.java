package activemq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class UsuarioConsumer {


    @Autowired
    private JmsTemplate jmsTemplate;

    Logger log = LoggerFactory.getLogger(UsuarioConsumer.class);

    public void processaUsuario(){

        try{
            log.info("Processamento de usuário Iniciado por EndPoint para  "+ Destinations.FILA_USUARIO);
            Usuario usuario = (Usuario) jmsTemplate.receiveAndConvert(Destinations.FILA_USUARIO);
            log.info("Usuario recebido : {}" , usuario.getNome());
        } catch(Exception e){
            log.error(" Ocorreu um erro durante envio da Mensagem: ", e);
        }
    }

}