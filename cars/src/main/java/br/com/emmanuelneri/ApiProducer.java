package br.com.emmanuelneri;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

@Component
public class ApiProducer {
    @Autowired
    private JmsTemplate jmsTemplate;

    Logger log = LoggerFactory.getLogger(ApiProducer.class);

    @Value("${pos.activemq.fila}")
    private String fila;

    public Boolean sendMessage(String message){
        try{
            log.info("Enviando mensagem para o tópico : "+ fila);

            jmsTemplate.convertAndSend(fila, message);
            log.info("Usuario enviado para o tópico : "+ fila);
        } catch(Exception e){
            log.error(" Ocorreu um erro durante envio da Mensagem: ", e);
            return false;
        }
        return true;
    }
}
