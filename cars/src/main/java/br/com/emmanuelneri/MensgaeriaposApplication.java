package br.com.emmanuelneri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MensgaeriaposApplication {

	public static void main(String[] args) {


		SpringApplication.run(MensgaeriaposApplication.class, args);
	}

}
