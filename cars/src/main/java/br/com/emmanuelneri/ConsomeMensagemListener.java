package br.com.emmanuelneri;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;


@Component
public class ConsomeMensagemListener {

    Logger log = LoggerFactory.getLogger(ConsomeMensagemListener.class);

    @JmsListener(destination = "topico.apis")
    public void processMessage(String msg) {

        log.info("Recebendo mensagem por Listener {} ", msg);
    }

}
