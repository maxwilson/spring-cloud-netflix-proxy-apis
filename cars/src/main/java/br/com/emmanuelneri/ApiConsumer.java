package br.com.emmanuelneri;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class ApiConsumer {

    @Autowired
    private JmsTemplate jmsTemplate;

    Logger log = LoggerFactory.getLogger(ApiConsumer.class);

    public void processaApi(){

        try{
            log.info("Processamento de usuário Iniciado por EndPoint para  "+ Destinations.FILA_USUARIO);
            jmsTemplate.receiveAndConvert(Destinations.FILA_USUARIO);
            log.info("Recebido");
        } catch(Exception e){
            log.error(" Ocorreu um erro durante envio da Mensagem: ", e);
        }
    }
}
