package br.com.emmanuelneri;

import com.google.common.collect.ImmutableMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cars")
public class CarController {
	@Autowired
	private ApiProducer apiProducer;
	@Autowired
	private ApiConsumer apiConsumer;
	
    private static final Map<Long, String> CARS = ImmutableMap.of(
            1L, "BMW ",
            2L, "Ferrari",
            3L, "FordCar",
            4L, "Gol");

    @GetMapping
    public List<String> findAll() {
    	apiProducer.sendMessage("Carros Gerados");
    	return new ArrayList<>(CARS.values());
    }

    @GetMapping(path = "/{id}")
    public String findById(@PathVariable("id") Long id) {
    	apiConsumer.processaApi();
    	return CARS.get(id);
    }

}
